//
//  ViewController.swift
//  HarryPotter_MS
//
//  Created by COTEMIG on 20/10/22.
//


import UIKit
import Alamofire
import Kingfisher

struct Personagem: Decodable {
    let name: String
    let actor: String
    let image: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDePersonagens:[Personagem] = []
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaDePersonagens.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let personagem = self.listaDePersonagens[indexPath.row]
  
        cell.nome.text = personagem.name
        cell.ator.text = personagem.actor
        cell.img.image = UIImage(named: personagem.image)
        
        cell.img.kf.setImage(with: URL(string: personagem.image))
        
        return cell
    }
        
    func getImagemPersonagem() {
        AF.request("https://hp-api.herokuapp.com/api/characters").responseDecodable(of: [Personagem].self) { response in
            if let personagem = response.value {
                self.listaDePersonagens = personagem
                
                
            }
            self.tableView.reloadData()
        }
    }


    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
    
        getImagemPersonagem()
    }
}

